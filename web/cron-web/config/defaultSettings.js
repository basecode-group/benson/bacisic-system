export default {
  navTheme: 'dark',
  // 拂晓蓝
  primaryColor: '#1890ff',
  layout: 'topmenu',
  contentWidth: 'Fixed',
  fixedHeader: true,
  autoHideHeader: false,
  fixSiderbar: false,
  colorWeak: false,
  menu: {
    locale: true,
  },
  title: 'Basic Cron',
  pwa: false,
  iconfontUrl: '',
  // http: 'http://localhost:8080'
  http: ''
};
