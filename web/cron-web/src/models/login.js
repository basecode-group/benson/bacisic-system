import { stringify } from 'querystring';
import { router } from 'umi';
import { fakeAccountLogin, fakeAccountLogout } from '@/services/login';
import { setAuthority } from '@/utils/authority';
import { getPageQuery } from '@/utils/utils';
const Model = {
  namespace: 'login',
  state: {
    status: undefined,
    currentUser: {}
  },
  effects: {
    *login({ payload }, { call, put }) {
      const response = yield call(fakeAccountLogin, payload);

      if (response.code === '000') {
        yield put({
          type: 'setCurrentUser',
          payload: response.result,
        });
        localStorage.setItem('Authorization', response.result.tails.access_token);
        window.location.href = '/cron/config';
        return;
      } else {
        yield put({
          type: 'changeLoginStatus',
          payload: "error",
        });
      }
    },

    *logout(_, { call }) {
      yield call(fakeAccountLogout);
      window.location.href="/login/cron";
    },
  },
  reducers: {
    changeLoginStatus(state, { payload }) {
      setAuthority(payload.currentAuthority);
      return { ...state, status: payload.status, type: payload.type };
    },
    setCurrentUser(state, { payload }){
      console.log(payload)
      return { ...state, currentUser: payload };
    }
  },
};
export default Model;
