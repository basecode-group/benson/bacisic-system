import request from '@/utils/request';
import defaultSettings from '../../config/defaultSettings';

const { http } = defaultSettings;

export async function query() {
  return request('/api/users');
}
export async function queryCurrent() {
  return request(http+'/sys/oauth/login/verify',{
    headers:{
      Authorization: localStorage.getItem('Authorization')
    }
  });
}
export async function queryNotices() {
  return request('/api/notices');
}
