import request from '@/utils/request';
import defaultSettings from '../../config/defaultSettings';

const { http } = defaultSettings;

export async function getCronList(params) {
  return request(`${http}/cron`,{
    method: 'Get',
    params: params,
    headers: {
      Authorization: localStorage.getItem('Authorization')
    }
  });
}

export async function getCronById(params) {
  return request(`${http}/cron/${params.id}`,{
    method: 'Get',
    headers: {
      Authorization: localStorage.getItem('Authorization')
    }
  })
}

export async function getCronSelectData() {
  return request(`${http}/cron/get/system/cron/data`,{
    headers: {
      Authorization: localStorage.getItem('Authorization')
    }
  })
}

export async function saveCron(params) {
  return request(`${http}/cron`,{
    method: 'POST',
    data: params,
    headers: {
      Authorization: localStorage.getItem('Authorization')
    }
  })
}

export async function updateCron(params) {
  return request(`${http}/cron/${params.id}`,{
    method: 'PUT',
    data: params,
    headers: {
      Authorization: localStorage.getItem('Authorization')
    }
  })
}

export async function deleteCron(params) {
  return request(`${http}/cron/${params.id}`,{
    method: 'DELETE',
    headers: {
      Authorization: localStorage.getItem('Authorization')
    }
  })
}
