import request from '@/utils/request';
import defaultSettings from '../../config/defaultSettings';

const { http } = defaultSettings;

export async function fakeAccountLogin(params) {
  return request(`${http}/sys/oauth/login?username=${params.username}&password=${params.password}`);
}

export async function fakeAccountLogout() {
  return request(`${http}/sys/oauth/logout`,{
    headers: {
      Authorization: localStorage.getItem('Authorization')
    }
  });
}
