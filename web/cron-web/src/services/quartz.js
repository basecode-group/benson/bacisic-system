import request from '@/utils/request';
import defaultSettings from '../../config/defaultSettings';

const { http } = defaultSettings;

// 查询下一期
export async function getQuartzNextDate(params) {
  return request(`${http}/quartz/next/cron/date`,{
    method: 'Get',
    params: params,
    headers: {
      Authorization: localStorage.getItem('Authorization')
    }
  });
}

// 获取定时任务详情
export async function getSchedule(params) {
  return request(`${http}/quartz/get/schedule/${params.jobName}`, {
    headers: {
      Authorization: localStorage.getItem('Authorization'),
    },
  })
}

// 创建定时任务
export async function createSchedule(params) {
  return request(`${http}/quartz/create/schedule/${params.jobName}`, {
    method: 'POST',
    headers: {
      Authorization: localStorage.getItem('Authorization'),
    },
  })
}

// 删除定时任务
export async function deleteSchedule(params) {
  return request(`${http}/quartz/delete/schedule/${params.jobName}`, {
    method: 'DELETE',
    headers: {
      Authorization: localStorage.getItem('Authorization'),
    },
  })
}

// 暂停定时任务
export async function pauseSchedule(params) {
  return request(`${http}/quartz/pause/schedule/${params.jobName}`, {
    method: 'POST',
    headers: {
      Authorization: localStorage.getItem('Authorization'),
    },
  })
}

// 恢复定时任务
export async function resumeSchedule(params) {
  return request(`${http}/quartz/resume/schedule/${params.jobName}`, {
    method: 'POST',
    headers: {
      Authorization: localStorage.getItem('Authorization'),
    },
  })
}

// 运行一次定时任务
export async function runOnceSchedule(params) {
  return request(`${http}/quartz/runOnce/schedule/${params.jobName}`, {
    method: 'POST',
    headers: {
      Authorization: localStorage.getItem('Authorization'),
    },
  })
}

// 运行一次定时任务
export async function updateSchedule(params) {
  return request(`${http}/quartz/update/schedule/${params.jobName}`, {
    method: 'PUT',
    data: params,
    requestType: 'form',
    headers: {
      Authorization: localStorage.getItem('Authorization'),
    },
  })
}

