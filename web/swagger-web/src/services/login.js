import request from '@/utils/request';
import defaultSettings from '../../config/defaultSettings';

const { http } = defaultSettings;

export async function fakeAccountLogin(params) {
  return request(`${http}/swagger/login?username=${params.username}&password=${params.password}`);
}

export async function fakeAccountLogout(params) {
  return request(`${http}/swagger/logout`);
}
export async function getFakeCaptcha(mobile) {
  return request(`/api/login/captcha?mobile=${mobile}`);
}
