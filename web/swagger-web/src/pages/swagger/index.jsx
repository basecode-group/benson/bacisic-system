import React, { Component } from 'react';
import { Form, Card } from 'antd';
import { connect } from 'dva';
import SwaggerUI from "swagger-ui-react"
import "swagger-ui-react/swagger-ui.css"
import defaultSettings from '../../../config/defaultSettings';

const { http } = defaultSettings;
// const http = "http://47.56.240.158:8020";
/**
 * Swagger UI
 *
 * @author zhangby
 * @date 19/2/20 6:10 pm
 */
class index extends Component {

  render() {
    const { swaggerData, authorization } = this.props;
    const url = `${swaggerData.selectedSwagger}`;
    return (
      <div>
        <Card style={{ background: '#fafafa'}}>
          <SwaggerUI
            key={url + authorization}
            url={url}
            docExpansion={"none"}
            onComplete={(swaggerUi) => {
              swaggerUi.preauthorizeApiKey('Authorization', authorization);
            }}
          />
        </Card>
      </div>
    )
  }
}

export default connect(({swagger }) => ({
  swaggerData: swagger,
  authorization: swagger.authorization
}))(index);
