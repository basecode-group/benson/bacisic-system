import { stringify } from 'querystring';
import { router } from 'umi';
import { fakeAccountLogin, getFakeCaptcha, fakeAccountLogout } from '@/services/login';
import { setAuthority } from '@/utils/authority';
import { getPageQuery } from '@/utils/utils';
const Model = {
  namespace: 'login',
  state: {
    status: undefined,
  },
  effects: {
    *login({ payload }, { call, put }) {
      const response = yield call(fakeAccountLogin, payload);

      if (response.code === '000') {
        window.location.href = '/swagger/api';
        return;
      } else {
        yield put({
          type: 'changeLoginStatus',
          payload: "error",
        });
      }
    },

    *getCaptcha({ payload }, { call }) {
      yield call(getFakeCaptcha, payload);
    },

    *logout({ payload }, { call}) {
      yield call(fakeAccountLogout, payload);
      window.location.href = '/login/swagger';
    },
  },
  reducers: {
    changeLoginStatus(state, { payload: status }) {
      return { ...state, status: status };
    },
  },
};
export default Model;
