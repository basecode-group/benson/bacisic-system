// src/access.ts
export default function access(initialState: {
  currentUser?: API.CurrentUser | undefined;
  menuAuth?: string[] | [];
}) {
  const { currentUser, menuAuth } = initialState || {};
  return {
    canAdminNot: false,
    canAdmin: (foo) => {
      if (menuAuth) {
        return menuAuth && menuAuth.indexOf(foo.path) > -1;
      }
      return true;
    },
    // canAdmin: currentUser && currentUser.access === 'admin',
  };
}
