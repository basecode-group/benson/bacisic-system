import React from 'react';
import { history } from 'umi';

/**
 * 跳转
 *
 * @author zhangby
 * @date 22/9/20 4:30 pm
 */
export default (): React.ReactNode => {
  history.push('/sys/user');
  return <></>;
};
