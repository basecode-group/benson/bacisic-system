import React, { useRef, useState } from 'react';
import { Button, Col, Drawer, message, Radio, Row, Spin, Switch, Tag } from 'antd';
import { UsergroupAddOutlined } from '@ant-design/icons';
import AntIcon from '@/pages/common/AntIcon';
import RoleMenu from '@/pages/system/role/components/RoleMenu';
import RoleUser from '@/pages/system/role/components/RoleUser';

// 定义传参
interface RoleAuthParams {
  role: System.SysRole;
}

// 参数
interface RoleAuthData {
  visible: boolean;
  loading: boolean;
  type: string;
}

/**
 * 角色授权管理
 *
 * @author zhangby
 * @date 24/9/20 11:34 am
 */
const RoleAuth: React.FC<RoleAuthParams> = (props) => {
  const { role } = props;
  // 初始化数据
  const [roleAuthData, setRoleAuthData] = useState<RoleAuthData>({
    visible: false,
    loading: false,
    type: '1',
  });

  // 设值
  const setData = (data: any) => {
    setRoleAuthData({ ...roleAuthData, ...data });
  };

  // 初始化
  const onPreInit = () => {
    setData({ visible: true, type: '1' });
  };

  const childRef = useRef();

  // 提交
  const onSubmit = async () => {
    try {
      setData({ loading: true });
      // @ts-ignore
      const data = await childRef.current.onSubmit();
      if (data.code === '000') {
        message.success('角色授权成功');
        setData({ loading: false, visible: false });
      }
    } catch (e) {
      setData({ loading: false });
    }
  };

  return (
    <>
      <Button type="primary" shape="circle" onClick={onPreInit}>
        <UsergroupAddOutlined />
      </Button>
      {/* 弹出窗口 */}
      <Drawer
        title="角色授权"
        placement="right"
        closable={false}
        onClose={() => setData({ visible: false })}
        visible={roleAuthData.visible}
        width={650}
        footer={
          <div style={{ textAlign: 'right' }}>
            <Button onClick={() => setData({ visible: false })} style={{ marginRight: 8 }}>
              取消
            </Button>
            <Button onClick={onSubmit} type="primary">
              确定
            </Button>
          </div>
        }
      >
        <Spin spinning={roleAuthData.loading}>
          <div style={{ marginBottom: 20 }}>
            <Radio.Group
              style={{ marginBottom: 16, marginLeft: 200 }}
              onChange={(val) => setData({ type: val.target.value })}
              value={roleAuthData.type}
            >
              <Radio.Button value="1">菜单授权</Radio.Button>
              <Radio.Button value="2">用户授权</Radio.Button>
            </Radio.Group>
          </div>

          <div>
            <h3>角色信息</h3>
            <Row style={{ marginTop: 30 }}>
              <Col span={12} style={{ marginBottom: 15 }}>
                <AntIcon
                  type="icon-yonghu"
                  style={{ fontSize: '1.5em', float: 'left', marginRight: 10 }}
                />
                <span> 角色名称：</span>
                <span> {role.name}</span>
              </Col>
              <Col span={12} style={{ marginBottom: 15 }}>
                <AntIcon
                  type="icon-yingwen"
                  style={{ fontSize: '1.4em', float: 'left', marginRight: 10, marginLeft: 2 }}
                />
                <span> 英文名称：</span>
                <span> {role.enname}</span>
              </Col>
              <Col span={12} style={{ marginBottom: 15 }}>
                <AntIcon
                  type="icon-biaoji"
                  style={{ fontSize: '1.5em', float: 'left', marginRight: 10 }}
                />
                <span> 角色类型：</span>
                <span>
                  <Tag color="magenta">{role.tails.roleTypeLabel}</Tag>
                </span>
              </Col>
              <Col span={12} style={{ marginBottom: 15 }}>
                <AntIcon
                  type="icon-jiandangzhuangtai-copy-copy"
                  style={{ fontSize: '1.7em', float: 'left', marginRight: 8 }}
                />
                <span> 是否可用：</span>
                <span>
                  <Switch size="small" checked={role.useable === '1'} />
                </span>
              </Col>
            </Row>
          </div>
          <div>
            {roleAuthData.type === '1' ? (
              <RoleMenu roleId={role.id} ref={childRef} />
            ) : (
              <RoleUser roleId={role.id} ref={childRef} />
            )}
          </div>
        </Spin>
      </Drawer>
    </>
  );
};

export default RoleAuth;
