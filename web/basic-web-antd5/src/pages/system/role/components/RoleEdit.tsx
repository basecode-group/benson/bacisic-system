import React, { useState } from 'react';
import { Button, Form, Input, Modal, Radio, Select } from 'antd';
import { EditOutlined } from '@ant-design/icons';
import { getDictList4Type } from '@/services/system/dict';
import { getRoleById, roleUpdate } from '@/services/system/role';

const { Option } = Select;

// 定义传参
interface RoleEditParams {
  refresh: () => void;
  id: string;
}

// 角色参数
interface RoleEditData {
  roleTypeList?: API.DictMap[];
}

/**
 * 新建角色
 *
 * @author zhangby
 * @date 23/9/20 4:41 pm
 */
const RoleEdit: React.FC<RoleEditParams> = (props) => {
  const { children, id, refresh } = props;
  const [loading, setLoading] = useState(false);
  const [visible, setVisible] = useState(false);
  const [roleData, setRoleData] = useState<RoleEditData>({});

  const [form] = Form.useForm();

  // 初始化
  const onPreInit = async () => {
    setVisible(true);
    form.resetFields();
    // 查询
    try {
      setLoading(true);
      const data = await getDictList4Type('role_type');
      if (data.code === '000') {
        // @ts-ignore
        setRoleData({ ...roleData, roleTypeList: data.result });
      }
      // 查询角色详情
      const role = await getRoleById(id);
      if (data.code === '000') {
        form.setFieldsValue(role.result);
        setLoading(false);
      }
    } catch (e) {
      setLoading(false);
    }
  };

  // 提交
  const onSubmit = () => {
    form.validateFields().then(async (values) => {
      try {
        setLoading(true);
        // @ts-ignore
        const data = await roleUpdate({ ...values, id });
        if (data.code === '000') {
          setLoading(false);
          setVisible(false);
          refresh();
        }
      } catch (e) {
        setLoading(false);
      }
    });
  };

  return (
    <>
      <span style={{ cursor: 'pointer' }} onClick={onPreInit}>
        {children || (
          <Button type="primary" shape="circle">
            <EditOutlined />
          </Button>
        )}
      </span>
      <Modal
        title="新建角色"
        visible={visible}
        onOk={onSubmit}
        onCancel={() => setVisible(false)}
        width={600}
      >
        <Form {...layout} form={form}>
          <Form.Item
            name="name"
            label="角色名称"
            rules={[{ required: true, message: '角色名称不能为空' }]}
          >
            <Input placeholder="角色名称" />
          </Form.Item>
          <Form.Item
            name="enname"
            label="英文名称"
            rules={[{ required: true, message: '英文名称不能为空' }]}
          >
            <Input placeholder="角色名称" />
          </Form.Item>
          <Form.Item name="roleType" label="角色类型">
            <Select placeholder="角色类型">
              {roleData.roleTypeList?.map((item, key) => (
                <Option key={key} value={item.value}>
                  {item.label}
                </Option>
              ))}
            </Select>
          </Form.Item>
          <Form.Item name="useable" label="状态" initialValue="1">
            <Radio.Group>
              <Radio value="1">启用</Radio>
              <Radio value="0">禁用</Radio>
            </Radio.Group>
          </Form.Item>
        </Form>
      </Modal>
    </>
  );
};

const layout = {
  labelCol: {
    xs: { span: 24 },
    sm: { span: 7 },
  },
  wrapperCol: {
    xs: { span: 24 },
    sm: { span: 12 },
  },
};

export default RoleEdit;
