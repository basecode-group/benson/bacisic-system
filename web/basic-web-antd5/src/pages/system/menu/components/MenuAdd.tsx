import React, { useState, useEffect } from 'react';
import { Button, Drawer, Spin, InputNumber, Input, message } from 'antd';
import { PlusOutlined } from '@ant-design/icons';
import ProForm, { ProFormText, ProFormSwitch } from '@ant-design/pro-form';
import SelectIcon from '@/pages/system/menu/components/SelectIcon';
import { menuSave, getMaxSort } from '@/services/system/menu';

// 定义传参
interface MenuAddParams {
  refresh: () => void;
  parentId?: string;
  parentName?: string;
}

/**
 * 新建菜单
 *
 * @author zhangby
 * @date 22/9/20 6:51 pm
 */
const MenuAdd: React.FC<MenuAddParams> = (props) => {
  const { parentId, parentName, children } = props;
  const [visible, setVisible] = useState(false);
  const [loading, setLoading] = useState(false);
  const [icon, setIcon] = useState<string>('');
  const [sort, setSort] = useState<number>(10);

  const [form] = ProForm.useForm();

  // 初始化
  const onPreInit = async () => {
    setVisible(true);
    form.resetFields();
    setIcon('');
    // 设置最大排序值
    try {
      setLoading(true);
      const {
        code,
        result: { maxSort },
      } = await getMaxSort(parentId);
      if (code === '000') {
        setSort(maxSort);
        setLoading(false);
      }
    } catch (e) {
      setLoading(false);
    }
  };

  // 提交
  const onSubmit = () => {
    // 验证
    form.validateFields().then(async (values) => {
      try {
        setLoading(true);
        const data = await menuSave({
          ...values,
          icon,
          parentId,
          sort,
          isShow: values.isShowBool ? '1' : '0',
        });
        if (data.code === '000') {
          setLoading(false);
          props.refresh();
          message.success('新建菜单成功！');
          setVisible(false);
        }
      } catch (e) {
        setLoading(false);
      }
    });
  };

  return (
    <>
      <span style={{ marginLeft: 10, cursor: 'pointer' }} onClick={onPreInit}>
        {children || (
          <Button type="primary">
            <PlusOutlined />
            新建菜单
          </Button>
        )}
      </span>
      {/* 弹出窗口 */}
      <Drawer
        title="新建菜单"
        placement="right"
        closable={false}
        onClose={() => setVisible(false)}
        visible={visible}
        width={650}
        footer={
          <div style={{ textAlign: 'right' }}>
            <Button onClick={() => setVisible(false)} style={{ marginRight: 8 }}>
              取消
            </Button>
            <Button onClick={onSubmit} type="primary">
              确定
            </Button>
          </div>
        }
      >
        <Spin spinning={loading}>
          <ProForm submitter={false} form={form}>
            {parentName ? (
              <div>
                <div style={{ marginBottom: 10 }}>是否显示</div>
                <Input value={parentName} disabled style={{ width: 400, marginBottom: 20 }} />
              </div>
            ) : null}
            <ProFormText
              name="name"
              label="菜单名称"
              placeholder="菜单名称"
              width={400}
              rules={[{ required: true, message: '菜单名称不能为空' }]}
            />
            <ProFormText
              name="href"
              label="链接"
              placeholder="链接"
              width={400}
              rules={[{ required: true, message: '链接不能为空' }]}
            />
            <div style={{ marginBottom: 10 }}>是否显示</div>
            <SelectIcon
              key={icon}
              value={icon}
              onChange={setIcon}
              style={{ width: 400, marginBottom: 20 }}
            />
            <ProFormSwitch name="isShowBool" label="是否显示" width={30} initialValue={true} />
            <div style={{ marginBottom: 10 }}>是否显示</div>
            <InputNumber
              min={0}
              step={1}
              precision={0}
              style={{ marginBottom: 20 }}
              defaultValue={10}
              value={sort}
              onChange={setSort}
            />
            <ProFormText name="target" label="目标路由" placeholder="目标路由" width={400} />
            <ProFormText name="permission" label="授权标识" placeholder="授权标识" width={400} />
          </ProForm>
        </Spin>
      </Drawer>
    </>
  );
};

export default MenuAdd;
