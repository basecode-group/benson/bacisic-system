import React, { useState, useEffect } from 'react';
import { TwitterPicker } from 'react-color';
import { Row, Col, Input } from 'antd';

// 定义传参
interface UserPhotoColorParams {
  onRender?: (item: any) => {};
}

interface UserFontData {
  fontColor?: string;
  fontValue?: string;
  background?: string;
}

/**
 * 用户头像选择
 *
 * @author zhangby
 * @date 25/9/20 11:34 am
 */
const UserPhotoColor: React.FC<UserPhotoColorParams> = (props) => {
  const { onRender } = props;
  const [userFontData, setUserFontData] = useState<UserFontData>({
    fontColor: '#ffffff',
    background: '#cccccc',
    fontValue: 'USER',
  });

  const setData = (data: UserFontData) => {
    const userFont = { ...userFontData, ...data };
    setUserFontData(userFont);
    onRender({ ...userFont, select: false, upload: false });
  };

  // const [fontColor, setFontColor] = useState<string>('');
  // const [fontValue, setFontValue] = useState<string>('#ffffff');
  // const [background, setBackground] = useState<string>('#cccccc');

  return (
    <>
      <Row gutter={10} style={{ marginTop: 20 }}>
        <Col md={12} xs={24} style={{ marginBottom: 10 }}>
          <div style={{ marginLeft: 60 }}>
            <TwitterPicker
              width={205}
              triangle="hide"
              colors={[...defaultColors, '#cccccc']}
              onChange={(color) => setData({ background: color.hex })}
              color={userFontData.background}
            />
            <div style={{ marginLeft: -45, fontWeight: 700, color: '#999', textAlign: 'center' }}>
              BackgroundColor
            </div>
          </div>
        </Col>
        <Col md={12} xs={24} style={{ marginBottom: 10 }}>
          <div style={{ marginLeft: 60 }}>
            <TwitterPicker
              width={205}
              triangle="hide"
              onChange={(color) => setData({ fontColor: color.hex })}
              colors={[...defaultColors, '#ffffff']}
              color={userFontData.fontColor}
            />
            <div style={{ marginLeft: -55, fontWeight: 700, color: '#999', textAlign: 'center' }}>
              FontColor
            </div>
          </div>
        </Col>
        <Col span={24} style={{ marginBottom: 20 }}>
          <Row>
            <Col
              span={11}
              style={{ textAlign: 'right', fontWeight: 700, color: '#999', marginTop: 5 }}
            >
              ICON:&nbsp;&nbsp;&nbsp;&nbsp;
            </Col>
            <Col span={12} style={{ textAlign: 'left' }}>
              <Input
                style={{ width: 100 }}
                placeholder="ICON"
                value={userFontData.fontValue}
                onChange={(e) => setData({ fontValue: e.target.value })}
              />
            </Col>
          </Row>
        </Col>
      </Row>
    </>
  );
};

// 基础颜色集
const defaultColors = [
  '#FF6900',
  '#FCB900',
  '#7BDCB5',
  '#00D084',
  '#8ED1FC',
  '#0693E3',
  '#00a2ae',
  '#EB144C',
  '#F78DA7',
  '#9900EF',
];

export default UserPhotoColor;
