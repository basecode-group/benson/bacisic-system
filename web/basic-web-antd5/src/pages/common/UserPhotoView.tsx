import React, { useEffect, useState } from 'react';
import icons12 from '@/assets/avatars/icons12.png';
import icons1 from '@/assets/avatars/1.jpg';
import icons2 from '@/assets/avatars/2.jpg';
import icons3 from '@/assets/avatars/3.jpg';
import icons4 from '@/assets/avatars/4.jpg';
import icons5 from '@/assets/avatars/5.jpg';
import icons6 from '@/assets/avatars/6.jpg';
import icons7 from '@/assets/avatars/7.jpg';
import icons8 from '@/assets/avatars/8.jpg';
import icons9 from '@/assets/avatars/9.png';
import icons10 from '@/assets/avatars/10.jpeg';
import icons11 from '@/assets/avatars/11.jpeg';
import Avatar from 'antd/lib/avatar/avatar';
import { UserOutlined } from '@ant-design/icons';
import { getDictList4Type } from '@/services/system/dict';

// 定义传参
interface UserPhotoViewParams {
  size?: number;
  photo?: {
    select?: boolean;
    key?: number;
    fontColor?: string;
    fontValue?: string;
    background?: string;
    upload?: boolean;
    photoUrl?: string;
  };
  photoStr?: string;
  style?: any;
}

/**
 * 用户头像预览
 *
 * @author zhangby
 * @date 25/9/20 11:03 am
 */
const UserPhotoView: React.FC<UserPhotoViewParams> = (props) => {
  const { size, photo, photoStr, style } = props;
  const [visitUrl, setVisitUrl] = useState<string>();
  const [photoData, setPhotoData] = useState<any>(photo);

  // 初始化
  useEffect(() => {
    // 判断是否已查询 -> 预览地址
    if (!visitUrl) {
      getDictList4Type('upload_config').then((res) => {
        if (res) {
          const dictMap = res.result?.filter((item) => item.label === 'img_visit_url')[0];
          if (dictMap) {
            setVisitUrl(dictMap.value);
          }
        }
      });
    }
    // 刷新对象
    try {
      if (photo) {
        setPhotoData(photo);
      }
      if (photoStr) {
        setPhotoData(JSON.parse(photoStr));
      }
    } catch (e) {}
  }, [photoStr, photo]);

  // 默认头像
  const defaultIcons = [
    <Avatar size={size} key={-2} icon={<UserOutlined />} />,
    <Avatar size={size} style={{ backgroundColor: '#00a2ae' }}>
      Admin
    </Avatar>,
    <Avatar size={size} style={{ backgroundColor: '#FF6900' }}>
      USER
    </Avatar>,
    <Avatar
      size={size}
      style={{
        color: '#f56a00',
        backgroundColor: '#fde3cf',
      }}
    >
      U
    </Avatar>,
    <Avatar size={size} src={icons12} />,
    <Avatar size={size} src={icons1} />,
    <Avatar size={size} src={icons2} />,
    <Avatar size={size} src={icons3} />,
    <Avatar size={size} src={icons4} />,
    <Avatar size={size} src={icons5} />,
    <Avatar size={size} src={icons6} />,
    <Avatar size={size} src={icons7} />,
    <Avatar size={size} src={icons8} />,
    <Avatar size={size} src={icons9} />,
    <Avatar size={size} src={icons10} />,
    <Avatar size={size} src={icons11} />,
  ];

  return (
    <span style={{ ...style }}>
      {photoData.upload ? (
        <Avatar size={size} src={visitUrl + photoData.photoUrl} />
      ) : photoData.select ? (
        defaultIcons[photoData.key || 0]
      ) : (
        <Avatar
          size={size}
          style={{
            color: photoData.fontColor,
            backgroundColor: photoData.background,
          }}
        >
          {photoData.fontValue}
        </Avatar>
      )}
    </span>
  );
};

// 初始化数据
UserPhotoView.defaultProps = {
  size: 35,
  photo: {
    select: true,
    key: 0,
    fontColor: '#ffffff',
    background: '#cccccc',
    fontValue: 'USER',
  },
};

export default UserPhotoView;
