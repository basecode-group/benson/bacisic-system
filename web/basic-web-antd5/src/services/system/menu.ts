import { request } from 'umi';
import { TableListParams } from '@/pages/ListTableList/data';

// 查询菜单列表
export async function getMenuList(params?: TableListParams) {
  const data = await request<API.ResultPoJo<System.SysMenu[]>>('/api/sys/menu', {
    params: {
      ...params,
      pageNum: params?.currentPage,
    },
    // token
    headers: {
      Authorization: localStorage.getItem('token'),
    },
  });
  const page = data.result;
  return {
    data: page,
    current: 1,
    total: page?.size,
    success: data.code === '000',
  };
}

// 保存菜单
export async function menuSave(params: System.SysMenu) {
  return request<API.ResultPoJo<any>>('/api/sys/menu', {
    method: 'post',
    data: params,
    // token
    headers: {
      Authorization: localStorage.getItem('token'),
    },
  });
}

// 获取最大排序值
export async function getMaxSort(parentId: string): Promise<API.ResultPoJo<any>> {
  return request(`/api/sys/menu/max/sort`, {
    params: { parentId },
    // token
    headers: {
      Authorization: localStorage.getItem('token'),
    },
  });
}

// 删除菜单
export async function menuDelete(id: string) {
  return request<API.ResultPoJo<any>>(`/api/sys/menu/${id}`, {
    method: 'delete',
    // token
    headers: {
      Authorization: localStorage.getItem('token'),
    },
  });
}

// 根据id 查询菜单
export async function getMenuById(id: string) {
  return request<API.ResultPoJo<System.SysMenu>>(`/api/sys/menu/${id}`, {
    // token
    headers: {
      Authorization: localStorage.getItem('token'),
    },
  });
}

// 更新菜单
export async function menuEdit(params: System.SysMenu) {
  return request<API.ResultPoJo<any>>(`/api/sys/menu/${params.id}`, {
    method: 'put',
    data: params,
    // token
    headers: {
      Authorization: localStorage.getItem('token'),
    },
  });
}

// 获取菜单记录
export async function getMenuList4Data(): Promise<API.ResultPoJo<System.SysMenu[]>> {
  // @ts-ignore
  return request('/api/sys/menu', {
    // token
    headers: {
      Authorization: localStorage.getItem('token'),
    },
  });
}

// 根据角色获取选中的ID
export async function getMenu4Role(roleId: string): Promise<API.ResultPoJo<string[]>> {
  // @ts-ignore
  return request(`/api/sys/menu/get/for/role/${roleId}`, {
    // token
    headers: {
      Authorization: localStorage.getItem('token'),
    },
  });
}

// 根据角色保存菜单
export async function saveMenu4Role(roleId: string, menuIds: string): Promise<API.ResultPoJo<any>> {
  // @ts-ignore
  return request(`/api/sys/menu/save/menu/role/${roleId}`, {
    method: 'post',
    data: { menuIds },
    requestType: 'form',
    // token
    headers: {
      Authorization: localStorage.getItem('token'),
    },
  });
}

// 加载菜单
export async function getMenu4User(): Promise<API.ResultPoJo<any>> {
  // @ts-ignore
  return request('/api/sys/menu/get/for/user', {
    method: 'get',
    // token
    headers: {
      Authorization: localStorage.getItem('token'),
    },
  });
}

// 加载授权菜单
export async function getMenu4Auth(): Promise<API.ResultPoJo<string[]>> {
  // @ts-ignore
  return request('/api/sys/menu/get/for/auth', {
    // token
    headers: {
      Authorization: localStorage.getItem('token'),
    },
  });
}
