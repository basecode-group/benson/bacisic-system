import { request } from 'umi';

// 查询角色
export async function getRoleList(params?: System.SystemParams): Promise<any> {
  const data = await request('/api/sys/role', {
    params: {
      ...params,
      pageNum: params?.currentPage,
    },
    // token
    headers: {
      Authorization: localStorage.getItem('token'),
    },
  });
  const page = data.result;
  return {
    data: page?.records,
    current: page?.current,
    total: page?.total,
    success: data.code === '000',
  };
}

// 新建角色
export async function roleAdd(params?: System.SysRole): Promise<API.ResultPoJo<any>> {
  // @ts-ignore
  return request('/api/sys/role', {
    method: 'post',
    data: params,
    // token
    headers: {
      Authorization: localStorage.getItem('token'),
    },
  });
}

// 删除角色
export async function roleDelete(id: string): Promise<API.ResultPoJo<any>> {
  // @ts-ignore
  return request<API.ResultPoJo<any>>(`/api/sys/role/${id}`, {
    method: 'delete',
    // token
    headers: {
      Authorization: localStorage.getItem('token'),
    },
  });
}

// 根据ID 查询角色
export async function getRoleById(id: string): Promise<API.ResultPoJo<System.SysRole>> {
  // @ts-ignore
  return request(`/api/sys/role/${id}`, {
    // token
    headers: {
      Authorization: localStorage.getItem('token'),
    },
  });
}

// 更新角色
export async function roleUpdate(params: System.SysRole): Promise<API.ResultPoJo<any>> {
  // @ts-ignore
  return request(`/api/sys/role/${params.id}`, {
    method: 'put',
    data: params,
    // token
    headers: {
      Authorization: localStorage.getItem('token'),
    },
  });
}

// 查询用户角色授权
export async function getRole4UserAuth(
  roleId: string,
): Promise<API.ResultPoJo<System.UserAuth4Role>> {
  // @ts-ignore
  return request(`/api/sys/role/get/user/auth/${roleId}`, {
    // token
    headers: {
      Authorization: localStorage.getItem('token'),
    },
  });
}

// 保存用户角色授权
export async function saveRole4UserAuth(
  roleId: string,
  userIds: string,
): Promise<API.ResultPoJo<any>> {
  // @ts-ignore
  return request(`/api/sys/role/save/user/auth/${roleId}`, {
    method: 'post',
    data: { userIds },
    requestType: 'form',
    // token
    headers: {
      Authorization: localStorage.getItem('token'),
    },
  });
}

// 查询用户角色
export async function getRoleSelectData(): Promise<API.ResultPoJo<API.DictMap[]>> {
  // @ts-ignore
  return request('/api/sys/role/select/data', {
    // token
    headers: {
      Authorization: localStorage.getItem('token'),
    },
  });
}
