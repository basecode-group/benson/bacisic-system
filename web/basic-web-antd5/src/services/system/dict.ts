import { request } from 'umi';
import { TableListParams } from '@/pages/ListTableList/data';

// 查询字典列表
export async function getDictList(params?: TableListParams) {
  const data = await request<API.ResultPoJo<API.Page<System.SysDict>>>('/api/sys/dict', {
    params: {
      ...params,
      pageNum: params?.currentPage,
    },
    // token
    headers: {
      Authorization: localStorage.getItem('token'),
    },
  });
  const page = data.result;
  return {
    data: page?.records,
    current: page?.current,
    total: page?.total,
    success: data.code === '000',
  };
}

// 查询字典详情
export async function getDictById(id: string) {
  return request<API.ResultPoJo<System.SysDict>>(`/api/sys/dict/${id}`, {
    // token
    headers: {
      Authorization: localStorage.getItem('token'),
    },
  });
}

// 获取最大排序值
export async function getMaxSort(parentId: string) {
  return request<API.ResultPoJo<System.SysSort>>('/api/sys/dict/max/sort', {
    params: { parentId },
    // token
    headers: {
      Authorization: localStorage.getItem('token'),
    },
  });
}

// 新建字典
export async function dictAdd(record: System.SysDict) {
  return request<API.ResultPoJo<any>>('/api/sys/dict', {
    method: 'POST',
    data: {
      ...record,
    },
    // token
    headers: {
      Authorization: localStorage.getItem('token'),
    },
  });
}

// 编辑字典
export async function dictEdit(record: System.SysDict) {
  return request<API.ResultPoJo<any>>(`/api/sys/dict/${record.id}`, {
    method: 'PUT',
    data: record,
    // token
    headers: {
      Authorization: localStorage.getItem('token'),
    },
  });
}

// 删除字典
export async function dictDelete(id: string) {
  return request<API.ResultPoJo<any>>(`/api/sys/dict/${id}`, {
    method: 'DELETE',
    // token
    headers: {
      Authorization: localStorage.getItem('token'),
    },
  });
}

// 查询字典数据
export async function getDictList4Type(type: string): Promise<API.ResultPoJo<API.DictMap[]>> {
  // @ts-ignore
  return request(`/api/sys/dict/select/data/${type}`, {
    // token
    headers: {
      Authorization: localStorage.getItem('token'),
    },
  });
}
