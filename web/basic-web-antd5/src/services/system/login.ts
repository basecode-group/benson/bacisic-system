import { request } from 'umi';

export interface LoginParamsType {
  username: string;
  password: string;
  mobile: string;
  captcha: string;
  type: string;
}

// 登录
export async function fakeAccountLogin(params: LoginParamsType) {
  return request<API.ResultPoJo<System.SysUser>>('/api/sys/oauth/login', {
    method: 'GET',
    params: {
      username: params.username,
      password: params.password,
    },
  });
}
export async function getFakeCaptcha(mobile: string) {
  return request(`/api/login/captcha?mobile=${mobile}`);
}

// 退出登录
export async function outLogin() {
  return request('/api/sys/oauth/logout', {
    headers: {
      Authorization: localStorage.getItem('token'),
    },
  });
}

// 验证登录状态
export async function loginVerify() {
  return request<API.ResultPoJo<string[]>>('/api/sys/oauth/login/verify', {
    headers: {
      Authorization: localStorage.getItem('token'),
    },
  });
}
