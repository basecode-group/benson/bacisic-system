export default {
  navTheme: 'dark',
  // navTheme: 'light',
  // 拂晓蓝
  primaryColor: '#2F54EB',
  layout: 'topmenu',
  contentWidth: 'Fixed',
  fixedHeader: true,
  autoHideHeader: false,
  fixSiderbar: false,
  colorWeak: false,
  menu: {
    locale: true,
  },
  title: 'Bacisic Creator',
  pwa: false,
  iconfontUrl: '',
  http: '',
  // http: 'http://localhost:9000',
  // http: 'http://localhost:8080',
};
