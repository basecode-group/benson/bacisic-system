# 基础架构

包含：
- 代码生成器  creator
- API文档    Swagger
- 授权模块    Security
- 基础代码    system
- 公共模块    common