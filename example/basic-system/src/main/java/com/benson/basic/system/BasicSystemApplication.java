package com.benson.basic.system;

import com.benson.basic.system.annotation.EnableAutoCorsConfig;
import com.benson.common.system.annotation.EnableSystemService;
import com.benson.common.upload.annotation.EnableUploadServer;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 基础系统启动项
 *
 * @author zhangby
 * @date 18/9/20 1:59 pm
 */
@EnableUploadServer
@EnableSystemService
@EnableAutoCorsConfig
@SpringBootApplication(scanBasePackages = {"com.benson.common.*","com.benson.basic.*"})
public class BasicSystemApplication {

    public static void main(String[] args) {
        SpringApplication.run(BasicSystemApplication.class, args);
    }

}
