package com.benson.basic.system.annotation;

import com.benson.basic.system.config.SysCorsConfig;
import org.springframework.boot.autoconfigure.AutoConfigurationPackage;
import org.springframework.context.annotation.Import;

import java.lang.annotation.*;

/**
 * 跨域配置
 *
 * @author zhangby
 * @date 18/9/20 2:28 pm
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Inherited
@AutoConfigurationPackage
@Import(SysCorsConfig.class)
public @interface EnableAutoCorsConfig {
}