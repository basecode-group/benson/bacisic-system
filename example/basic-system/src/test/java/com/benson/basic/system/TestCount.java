package com.benson.basic.system;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.io.FileUtil;
import cn.hutool.core.lang.Dict;
import cn.hutool.core.text.StrSpliter;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.google.common.collect.Lists;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class TestCount {
    public static void main(String[] args) {
        List<Dict> ipLine = Lists.newArrayList();
//        String log = "[INFO ] 2020-10-04 16:37:41.787 [reactor-http-epoll-6] []  API_LOGS - [2020-10-04 16:37:41:041]  8.210.150.88 -> /api/pay/win/number/44e6b68cd3ce41dda5dd972146357d1f/vietnam/vinhLong  -> {}";
        FileUtil.readLines("/Users/zhangbiyu/Documents/other/logs/logs/api/api-logs-2020-10-04.5.log", "utf-8")
                .parallelStream()
                .map(item -> {
                    String time = StrSpliter.split(item.replace("[INFO ] ", ""), " [", true, true).get(0);
                    return DateUtil.format(DateUtil.parseDateTime(time), "MM-dd HH");
                })
                .filter(StrUtil::isNotBlank)
                .collect(Collectors.groupingByConcurrent(item -> item))
                .forEach((item,li) -> {
                    ipLine.add(Dict.create()
                            .set("ip", item)
                            .set("count", li.size())
                    );
                });
        FileUtil.readLines("/Users/zhangbiyu/Documents/other/logs/logs/api/api-logs-2020-10-05.0.log", "utf-8")
                .parallelStream()
                .map(item -> {
                    String time = StrSpliter.split(item.replace("[INFO ] ", ""), " [", true, true).get(0);
                    return DateUtil.format(DateUtil.parseDateTime(time), "MM-dd HH");
                })
                .filter(StrUtil::isNotBlank)
                .collect(Collectors.groupingByConcurrent(item -> item))
                .forEach((item,li) -> {
                    ipLine.add(Dict.create()
                            .set("ip", item)
                            .set("count", li.size())
                    );
                });
        FileUtil.readLines("/Users/zhangbiyu/Documents/other/logs/logs/api/api-logs-2020-10-05.1.log", "utf-8")
                .parallelStream()
                .map(item -> {
                    String time = StrSpliter.split(item.replace("[INFO ] ", ""), " [", true, true).get(0);
                    return DateUtil.format(DateUtil.parseDateTime(time), "MM-dd HH");
                })
                .filter(StrUtil::isNotBlank)
                .collect(Collectors.groupingByConcurrent(item -> item))
                .forEach((item,li) -> {
                    Dict dict = ipLine.stream().filter(ite -> ite.getStr("ip").equals(item))
                            .findFirst().orElse(null);
                    int count = li.size();
                    if (ObjectUtil.isNotNull(dict)) {
                        count = count + dict.getInt("count");
                        ipLine.remove(dict);
                    }
                    ipLine.add(Dict.create()
                            .set("ip", item)
                            .set("count", count)
                    );
                });

        ipLine.stream()
                .sorted(Comparator.comparing(item -> item.toString()))
                .forEach(item -> {
                    System.out.println(item.get("ip") +"  ->  " + item.getInt("count"));
                });
//        List<List<String>> rows1 = FileUtil.readLines("/Users/zhangbiyu/Documents/other/logs/logs/lottery-gateway.log", "utf-8")
//                .parallelStream()
//                .filter(item -> StrUtil.isNotBlank(item) && item.contains("?????"))
//                .map(item -> item.split("       : ")[1].replace("?????", ""))
//                .map(item -> {
//                    List<String> row = Lists.newArrayList();
//                    String[] item1 = item.split(" - - ");
//                    String[] item2 = item1[1].split("] ");
//                    String time = item2[0].replace("[", "");
//                    row.add(time.substring(0, time.lastIndexOf(":")));
//                    row.add(item1[0]);
//                    row.add(item2[1].trim());
//                    return row;
//                })
//                .collect(Collectors.toList());
//
//        List<List<String>> rows = FileUtil.readLines("/Users/zhangbiyu/Documents/other/logs/logs/lottery-gateway-2020-09-29.4.log", "utf-8")
//                .parallelStream()
//                .filter(item -> StrUtil.isNotBlank(item) && item.contains("?????"))
//                .map(item -> item.split("       : ")[1].replace("?????", ""))
//                .map(item -> {
//                    List<String> row = Lists.newArrayList();
//                    String[] item1 = item.split(" - - ");
//                    String[] item2 = item1[1].split("] ");
//                    String time = item2[0].replace("[", "");
//                    row.add(time.substring(0, time.lastIndexOf(":")));
//                    row.add(item1[0]);
//                    row.add(item2[1].trim());
//                    return row;
//                })
//                .collect(Collectors.toList());
//
//        rows.addAll(rows1);
//
//        //通过工具类创建writer
//        ExcelWriter writer = ExcelUtil.getWriter("/Users/zhangbiyu/Documents/other/logs/logs/lottery-gateway-29.xlsx");
//        //通过构造方法创建writer
//        //ExcelWriter writer = new ExcelWriter("d:/writeTest.xls");
//
//        //跳过当前行，既第一行，非必须，在此演示用
//        writer.passCurrentRow();
//
//        //合并单元格后的标题行，使用默认标题样式
////        writer.merge(row1.size() - 1, "测试标题");
//        //一次性写出内容，强制输出标题
//        writer.write(rows, true);
//        //关闭writer，释放内存
//        writer.close();
    }
}
