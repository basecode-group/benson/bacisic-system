package com.benson.basic.system;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.io.FileUtil;
import cn.hutool.core.lang.Dict;
import cn.hutool.core.text.StrSpliter;
import cn.hutool.core.util.StrUtil;

import java.io.File;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class LogCount {
    public static void main(String[] args) {
        Map<String, Integer> countMap = new HashMap();

        File[] files = FileUtil.ls("/Users/zhangbiyu/Documents/other/logs/logs/web/");
        Stream.of(files)
                .filter(fl -> fl.getName().contains("2020-10-04"))
                .forEach(fl -> {
                    FileUtil.readLines(fl, "utf-8")
                            .parallelStream()
                            .map(item -> {
                                String time = StrSpliter.split(item.replace("[INFO ] ", ""), " [", true, true).get(0);
                                return DateUtil.format(DateUtil.parseDateTime(time), "MM-dd HH");
                            })
                            .filter(StrUtil::isNotBlank)
                            .collect(Collectors.groupingByConcurrent(item -> item))
                            .forEach((item,li) -> {
                                Integer count = li.size();
                                if (countMap.containsKey(item)) {
                                    count = count + countMap.get(item);
                                }
                                countMap.put(item, count);
                            });
                });

        countMap.keySet().stream()
                .sorted(Comparator.comparing(Object::toString))
                .forEach(item -> {
                    System.out.println(item + " -> " + countMap.get(item));
                });
    }
}
