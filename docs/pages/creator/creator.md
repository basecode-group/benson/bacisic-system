# 代码生成器模块

代码生成器是基于 [Mybatis-plus](https://mybatis.plus/guide/generator.html#%E4%BD%BF%E7%94%A8%E6%95%99%E7%A8%8B) 封装的代码生成模块，
可自动生成，`controller`、`service`、`serviceImpl`、`mapper`、`xml`。

如果对代码生成有特殊配置，可自行修改 `com.benson.common.creator.service.impl.CreatorServiceImpl.java`。

?> 后端服务启动后，访问地址：`http://localhost:8080/code/generator`。

### 配置代码生成。

- 一、代码生成管理
<img src="./images/creator-01.png" style="width:780px;border: 1px solid #9e9e9e29;" class="no-zoom" />

- 二、选择生成的表
<img src="./images/creator-03.png" style="width:480px;border: 1px solid #9e9e9e29;" class="no-zoom" />

- 三、选择生成的类
<img src="./images/creator-04.png" style="width:480px;border: 1px solid #9e9e9e29;" class="no-zoom" />
