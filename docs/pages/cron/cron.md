# 定时任务模块

定时任务是基于`spring-boot-starter-quartz`封装的，支持动操作定时任务，包含：`创建`、`删除`、`执行`、`暂停`、`恢复`、`修改`。

?> 后端服务启动后，访问地址：`http://localhost:8080/cron/config`。

### 后端配置。

#### 一、集成并实现调用接口`BaseCron`。

```java
package com.benson.basic.system.cron;

import cn.hutool.core.date.DateTime;
import com.benson.common.cron.annotation.CronConfig;
import com.benson.common.cron.cron.BaseCron;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

/**
 * 系统定时任务
 *
 * @author zhangby
 * @date 9/10/20 11:09 am
 */
@CronConfig(cron = "* * * * * ?", value = "SysCronMinute", description = "系统默认定时任务")
public class SysCronMinute extends BaseCron {

    /**
     * 定时任务实现接口
     */
    @Override
    protected void executeInternal(JobExecutionContext jobExecutionContext) throws JobExecutionException {
        System.out.println(new DateTime());
    }
}
```

> 增加 `@CronConfig` 注解，设置`cron`。

#### 二、配置cron扫描包。

``` yaml
## basic-common config
basic-common:
  cron:
    scan-package: com.benson.basic.system.cron
```

### 页面设置。

#### 一、访问定时任务配置页面。

<img src="./images/cron-01.png" style="width:780px;border: 1px solid #9e9e9e29;" class="no-zoom" />

#### 二、选择执行的定时任务

!> 如果不设置，会按照代码里设置的定时任务执行。如果这里增加了控制的话，会按照此页面设置的执行。

- 创建定时任务
<img src="./images/cron-02.png" style="width:780px;border: 1px solid #9e9e9e29;" class="no-zoom" />

- 设置 Cron
<img src="./images/cron-03.png" style="width:780px;border: 1px solid #9e9e9e29;" class="no-zoom" />

#### 三、控制定时任务

!> 定时任务设置后，会自动同步。定时任务执行Cron已当前设置优先。

- 控制定时任务
<img src="./images/cron-04.png" style="width:780px;border: 1px solid #9e9e9e29;" class="no-zoom" />
