# Swagger API文档模块

Swagger API文档，是基于Swagger去做的封装，同时增加了登录配置的功能。

?> 后端服务启动后，访问地址：`http://localhost:8080/swagger/api`。

### 后端配置

#### 一、Springboot 引入Swagger。

Swagger 配置省略，参考：[Springboot安装Swagger](http://regan_jeff.gitee.io/benson_projects/#/tools/swagger/swagger)

> Maven不需要引入 `springfox-swagger-ui`。

#### 二、访问swagger地址。

- swagger 管理

<img src="./images/swagger-01.png" style="width:780px;border: 1px solid #9e9e9e29;" class="no-zoom" />

- 增加自动token授权管理
<img src="./images/swagger-02.png" style="width:780px;border: 1px solid #9e9e9e29;" class="no-zoom" />

- token授权设置
<img src="./images/swagger-03.png" style="width:780px;border: 1px solid #9e9e9e29;" class="no-zoom" />
