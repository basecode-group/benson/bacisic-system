# System 模块运行

#### 一、后端代码运行。

- 1. 下载example，基础模块。
- 2. maven 编译
- 3. mysql 安装基础数据。
- 4. 修改配置文件：mysql、redis。

#### 二、Web前端代码运行。

- 1. 下载源码，并编译: `yarn`。

- 2. 修改前端代理（ `/config/proxy.ts` ）。

``` javascript
export default {
  dev: {
    '/api/': {
      target: 'http://localhost:8080',
      changeOrigin: true,
      pathRewrite: { '/api': '' },
    },
  },
}
```
> 修改代理服务地址 `http://localhost:8080`。

- 3. 启动服务：`yarn start`。

#### 登录

默认用户名密码：`admin`/`123456`。
