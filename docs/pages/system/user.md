# 系统管理模块

### 一、用户管理模块

- 用户管理
<img src="./images/system-01.png" style="width:780px;border: 1px solid #9e9e9e29;" class="no-zoom" />

- 设置头像
<img src="./images/system-photo-01.png" style="width:380px;border: 1px solid #9e9e9e29;" class="no-zoom" />
<img src="./images/system-photo-02.png" style="width:380px;border: 1px solid #9e9e9e29;" class="no-zoom" />
<img src="./images/system-photo-03.png" style="width:380px;border: 1px solid #9e9e9e29;" class="no-zoom" />

### 二、角色管理模块

- 一、角色模块
<img src="./images/system-02.png" style="width:780px;border: 1px solid #9e9e9e29;" class="no-zoom" />

- 二、角色授权
<img src="./images/system-role-01.png" style="width:780px;border: 1px solid #9e9e9e29;" class="no-zoom" />
<img src="./images/system-role-02.png" style="width:780px;border: 1px solid #9e9e9e29;" class="no-zoom" />

### 三、菜单管理模块

- 一、菜单模块
<img src="./images/system-03.png" style="width:780px;border: 1px solid #9e9e9e29;" class="no-zoom" />

- 二、图标设置
<img src="./images/system-menu-01.png" style="width:780px;border: 1px solid #9e9e9e29;" class="no-zoom" />

### 三、字典管理模块

- 一、字典模块
<img src="./images/system-04.png" style="width:780px;border: 1px solid #9e9e9e29;" class="no-zoom" />

- 二、字典设置
<img src="./images/system-dict-01.png" style="width:780px;border: 1px solid #9e9e9e29;" class="no-zoom" />
