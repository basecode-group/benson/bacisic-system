package com.benson.common.cron.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import java.util.Date;

import com.benson.common.common.annotation.EnumFormat;
import com.benson.common.common.entity.BaseEntity;
import java.io.Serializable;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.benson.common.common.enums.StatusEnum;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 定时任务表
 * </p>
 *
 * @author zhangby
 * @since 2020-02-24
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("sys_cron")
@ApiModel(value="Cron对象", description="定时任务表")
public class Cron extends BaseEntity<Cron> {

private static final long serialVersionUID=1L;

    private String id;

    @ApiModelProperty(value = "定时任务名称")
    private String name;

    @ApiModelProperty(value = "定时任务标识")
    private String mark;

    @ApiModelProperty(value = "Cron 表达式")
    private String cron;

    @EnumFormat
    @ApiModelProperty(value = "状态： 0 正常， 1. 停用")
    private StatusEnum status;

    @ApiModelProperty(value = "关联id")
    private String actionId;

    @ApiModelProperty(value = "创建者",hidden = true)
    private String createBy;

    @ApiModelProperty(value = "创建时间",example = "2020-02-24 00:00:00",hidden = true)
    private Date createDate;

    @ApiModelProperty(value = "更新者",hidden = true)
    private String updateBy;

    @ApiModelProperty(value = "更新时间",example = "2020-02-24 00:00:00",hidden = true)
    private Date updateDate;

    @ApiModelProperty(value = "备注信息")
    private String remarks;

    @TableLogic
    @ApiModelProperty(value = "删除标记",example = "0",hidden = true)
    private String delFlag;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
