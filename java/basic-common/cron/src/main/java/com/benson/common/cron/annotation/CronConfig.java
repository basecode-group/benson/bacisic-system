package com.benson.common.cron.annotation;

import org.springframework.context.annotation.Import;
import org.springframework.stereotype.Component;

import java.lang.annotation.*;

/**
 * 定时任务配置 注解
 *
 * @author zhangby
 * @date 24/2/20 11:06 am
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Inherited
@Component
public @interface CronConfig {
    /** CRON 表达式，如果为空，需要手动设置 */
    String cron();

    /** 定时任务标识 */
    String value();

    /** 定时任务描述 */
    String description();
}
