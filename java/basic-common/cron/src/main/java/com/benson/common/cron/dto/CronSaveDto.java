package com.benson.common.cron.dto;

import com.benson.common.common.entity.BaseDto;
import com.benson.common.common.enums.StatusEnum;
import com.benson.common.common.util.CommonUtil;
import com.benson.common.common.util.EnumUtil;
import com.benson.common.cron.entity.Cron;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;
import org.springframework.beans.BeanUtils;

import javax.validation.constraints.NotBlank;

/**
 * 定时任务 保存Dto
 *
 * @author zhangby
 * @date 28/2/20 4:30 pm
 */
@Data
@Accessors(chain = true)
public class CronSaveDto extends BaseDto<Cron> {

    @NotBlank(message = "定时任务名称不能为空")
    @ApiModelProperty(value = "定时任务名称")
    private String name;

    @NotBlank(message = "定时任务名称不能为空")
    @ApiModelProperty(value = "定时任务标识")
    private String mark;

    @NotBlank(message = "Cron 表达式不能为空")
    @ApiModelProperty(value = "Cron 表达式")
    private String cron;

    @ApiModelProperty(value = "状态： 0 正常， 1. 停用")
    private String status;

    @ApiModelProperty(value = "备注信息")
    private String remarks;

    @Override
    public Cron convert() {
        Cron cron = new Cron();
        BeanUtils.copyProperties(this,cron);
        // 格式化状态
        StatusEnum statusEnum = CommonUtil.notEmpty(status)
                .map(item -> EnumUtil.initEnum(StatusEnum.class, item))
                .orElse(StatusEnum.ENABLE);
        cron.setStatus(statusEnum);
        return cron;
    }
}
