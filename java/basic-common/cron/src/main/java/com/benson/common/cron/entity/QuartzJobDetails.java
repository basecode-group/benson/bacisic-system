package com.benson.common.cron.entity;

import com.benson.common.common.annotation.EnumFormat;
import com.benson.common.cron.entity.enums.TriggerStateEnum;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * 定时任务详情
 *
 * @author zhangby
 * @date 19/12/19 11:04 am
 */
@Data
@Accessors(chain = true)
public class QuartzJobDetails {
    private String jobName;
    private String jobType;
    private String cron;
    @EnumFormat
    private TriggerStateEnum triggerStateEnum;
}
