package com.benson.common.cron.entity;

import lombok.Data;
import lombok.experimental.Accessors;

/**
 * 定时任务初始数据
 *
 * @author zhangby
 * @date 9/10/20 11:59 am
 */
@Data
@Accessors(chain = true)
public class CronInitEntity {
    /** CRON 表达式，如果为空，需要手动设置 */
    private String cron;

    /** 定时任务标识 */
    private String value;

    /** 定时任务描述 */
    private String description;
}
