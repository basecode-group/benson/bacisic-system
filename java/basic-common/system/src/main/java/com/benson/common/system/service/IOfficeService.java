package com.benson.common.system.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.benson.common.system.entity.Office;

/**
 * <p>
 * 职位表 服务类
 * </p>
 *
 * @author zhangbiyu
 * @since 2019-11-26
 */
public interface IOfficeService extends IService<Office> {

}
