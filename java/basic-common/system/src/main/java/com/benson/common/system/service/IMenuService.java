package com.benson.common.system.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.benson.common.system.entity.Menu;

import java.util.List;

/**
 * <p>
 * 菜单表 服务类
 * </p>
 *
 * @author zhangbiyu
 * @since 2019-11-27
 */
public interface IMenuService extends IService<Menu> {

    List<Menu> getMenu4User(String currentUserId);
}
