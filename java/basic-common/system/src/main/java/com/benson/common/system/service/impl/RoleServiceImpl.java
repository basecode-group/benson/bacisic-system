package com.benson.common.system.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.benson.common.system.entity.Role;
import com.benson.common.system.entity.enums.DictTypeEnum;
import com.benson.common.system.mapper.RoleDao;
import com.benson.common.system.service.IRoleService;
import com.benson.common.system.util.DictUtil;
import org.springframework.stereotype.Service;

import java.util.function.Function;

/**
 * <p>
 * 'sys.statements_with_temp_tables' is not BASE TABLE 服务实现类
 * </p>
 *
 * @author zhangbiyu
 * @since 2019-11-22
 */
@Service
public class RoleServiceImpl extends ServiceImpl<RoleDao, Role> implements IRoleService {

    /**
     * 初始化
     * @return
     */
    @Override
    public Function<Role, Role> preInit() {
        return role -> {
            //查询角色类型
            role.set("roleTypeLabel", DictUtil.getDictLabel(DictTypeEnum.roleType, role.getRoleType()));
            return role;
        };
    }
}
