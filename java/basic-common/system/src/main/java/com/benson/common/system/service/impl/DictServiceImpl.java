package com.benson.common.system.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.benson.common.system.entity.Dict;
import com.benson.common.system.mapper.DictDao;
import com.benson.common.system.service.IDictService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 'sys.statements_with_temp_tables' is not BASE TABLE 服务实现类
 * </p>
 *
 * @author zhangbiyu
 * @since 2019-11-23
 */
@Service
public class DictServiceImpl extends ServiceImpl<DictDao, Dict> implements IDictService {

}
