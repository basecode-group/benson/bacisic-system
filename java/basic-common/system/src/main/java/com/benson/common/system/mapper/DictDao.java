package com.benson.common.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.benson.common.system.entity.Dict;
import org.springframework.stereotype.Repository;

/**
 * <p>
 * 'sys.statements_with_temp_tables' is not BASE TABLE Mapper 接口
 * </p>
 *
 * @author zhangbiyu
 * @since 2019-11-23
 */
@Repository
public interface DictDao extends BaseMapper<Dict> {

}
