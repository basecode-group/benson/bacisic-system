package com.benson.common.system.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.benson.common.system.entity.Office;
import com.benson.common.system.mapper.OfficeDao;
import com.benson.common.system.service.IOfficeService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 职位表 服务实现类
 * </p>
 *
 * @author zhangbiyu
 * @since 2019-11-26
 */
@Service
public class OfficeServiceImpl extends ServiceImpl<OfficeDao, Office> implements IOfficeService {

}
