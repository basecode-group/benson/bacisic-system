package com.benson.common.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.benson.common.system.entity.Dict;

/**
 * <p>
 * 'sys.statements_with_temp_tables' is not BASE TABLE 服务类
 * </p>
 *
 * @author zhangbiyu
 * @since 2019-11-23
 */
public interface IDictService extends IService<Dict> {

}
