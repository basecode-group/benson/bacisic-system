package com.benson.common.system;

import com.benson.common.security.annotation.EnableJwtInterceptor;
import com.benson.common.security.annotation.EnableResource;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 基础数据服务
 *
 * @author zhangby
 * @date 17/2/20 5:51 pm
 */
@EnableResource
@EnableJwtInterceptor
@SpringBootApplication(scanBasePackages = "com.benson.common.*")
@MapperScan("com.benson.common.system.mapper")
public class SystemApplication {

    public static void main(String[] args) {
        SpringApplication.run(SystemApplication.class, args);
    }

}
