package com.benson.common.common.enums;

/**
 * 状态枚举
 *
 * @author zhangby
 * @date 15/1/20 4:54 pm
 */
public enum StatusEnum implements BaseEnum {
    /**
     * 启用
     */
    ENABLE("启用", "0"),
    /**
     * 禁用
     */
    DISABLE("禁用", "1"),
    ;

    private String label;
    private String value;

    StatusEnum(String label, String value) {
        this.label = label;
        this.value = value;
    }

    @Override
    public String getLabel() {
        return this.label;
    }

    @Override
    public String getValue() {
        return this.value;
    }
}
