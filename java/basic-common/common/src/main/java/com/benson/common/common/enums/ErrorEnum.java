package com.benson.common.common.enums;

import com.fasterxml.jackson.annotation.JsonValue;

/**
 * 错误类型
 *
 * @param <T>
 * @author zhangby
 * @date 27/9/19 6:03 pm
 */
public interface ErrorEnum<T extends ErrorEnum>{

    String getMsg();

    String getCode();

    /**
     * 格式化枚举
     *
     * @return
     */
    @JsonValue
    default String getEnums() {
        return getCode();
    }

}