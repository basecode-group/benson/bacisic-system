package com.benson.common.common.exception;

import com.benson.common.common.exception.code.BaseExceptionCode;
import com.benson.common.common.util.EnumUtil;

/**
 * 自定义异常
 *
 * @author zhangby
 * @date 2017/11/30 下午7:10
 */
public class MyBaselogicException extends RuntimeException {
    private static final long serialVersionUID = -6317037305924958356L;
    /**
     * 错误代码
     */
    private BaseExceptionCode error;
    /**
     * msg参数
     */
    private Object[] msg;

    public MyBaselogicException(String num) {
        this.error = EnumUtil.initErrorEnum(BaseExceptionCode.class, num);
    }

    public MyBaselogicException(BaseExceptionCode error) {
        this.error = error;
    }

    public MyBaselogicException(String num, Object... msg) {
        this.error = EnumUtil.initErrorEnum(BaseExceptionCode.class, num);
        this.msg = msg;
    }

    public MyBaselogicException(BaseExceptionCode error, Object... msg) {
        this.error = error;
        this.msg = msg;
    }

    public BaseExceptionCode getError() {
        return error;
    }

    public void setError(BaseExceptionCode error) {
        this.error = error;
    }

    public Object[] getMsg() {
        return msg;
    }

    public void setMsg(Object[] msg) {
        this.msg = msg;
    }
}
