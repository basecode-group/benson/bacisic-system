package com.benson.common.common.entity;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * 读取系统配置文件
 *
 * @author zhangby
 * @date 27/9/19 10:22 am
 */
@Component
@Data
public class SystemConfig {
    /**
     * 端口
     */
    @Value("${server.port:8080}")
    private String prot;

    /**
     * jwt 秘钥
     */
    @Value("${system.security_jwt_key:common_security}")
    private String jwtKey;


//    @Value("${system.security_filter_url}")
//    private List<String> filterUrl;
}