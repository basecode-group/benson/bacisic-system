package com.benson.common.common.exception.code;

import cn.hutool.core.lang.Dict;
import cn.hutool.json.JSONUtil;
import com.benson.common.common.enums.ErrorEnum;

/**
 * 基础异常 Code
 *
 * @author zhangby
 * @date 17/2/20 3:38 pm
 */
public enum BaseExceptionCode implements ErrorEnum {
    /**
     * 系统异常
     */
    OTHER_EXCEPTION("{}", "999"),
    LOGIN_TIMEOUT("登录超时", "998"),
    SYSTEM_EXCEPTION("系统异常请稍后重试", "997"),
    code_996("非ip白名单，没有权限", "996"),
    code_401("非授权访问，无效的token", "401"),
    code_402("token 已过期", "402"),
    code_403("权限不足，访问失败", "403"),
    code_404("权限不足，非白名单用户", "404"),
    code_100("用户不存在", "100"),
    code_101("验证码错误", "101"),
    code_102("登录失败请稍后重试", "102"),
    code_103("密码不正确", "103"),
    code_104("参数不能为空：[{}]", "104"),
    code_105("参数异常：[{}]", "105"),
    code_106("修改提交记录失败，状态异常", "106"),
    code_107("操作失败，记录不存在", "107"),
    code_108("用户名已存在", "108"),
    code_109("当前账号已停用", "109"),
    code_110("操作失败，{}", "110"),
    code_112("角色英文名已存在", "112"),
    code_113("自动开奖失败，请执行人工开奖", "113"),
    code_114("创建奖期失败，期号已存在", "114"),
    code_115("抓奖超时，请稍后重试", "115"),
    code_116("自动开奖失败，开奖日期被排除", "116"),
    code_117("操作失败，开奖号码格式异常", "117"),
    code_118("已点赞，不可重复操作", "118");

    private String msg;
    private String code;

    BaseExceptionCode(String msg, String code) {
        this.msg = msg;
        this.code = code;
    }

    @Override
    public String getMsg() {
        return this.msg;
    }

    @Override
    public String getCode() {
        return this.code;
    }

    @Override
    public String toString() {
        return JSONUtil.parseObj(Dict.create()
                .set("code", this.code)
                .set("msg", this.msg)
        ).toString();
    }
}
