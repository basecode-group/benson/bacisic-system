package com.benson.common.upload.annotation;

import com.benson.common.upload.UploadApplication;
import org.springframework.context.annotation.Import;

import java.lang.annotation.*;

/**
 * 启动定时任务服务
 *
 * @author zhangby
 * @date 24/2/20 2:02 pm
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Inherited
@Import({UploadApplication.class})
public @interface EnableUploadServer {
}
