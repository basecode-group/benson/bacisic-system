package com.benson.common.creator.mapper;

import com.benson.common.creator.entity.Creator;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;

/**
 * <p>
 * 代码生成 Mapper 接口
 * </p>
 *
 * @author zhangby
 * @since 2020-02-18
 */
@Repository
public interface CreatorDao extends BaseMapper<Creator> {

}
