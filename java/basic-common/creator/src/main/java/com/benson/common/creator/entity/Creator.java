package com.benson.common.creator.entity;

import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 代码生成
 * </p>
 *
 * @author zhangby
 * @since 2020-02-18
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("sys_creator")
public class Creator extends Model<Creator> {

private static final long serialVersionUID=1L;

    private String id;

    private String name;

    private String tableName;

    private String description;

    private String author;

    private String outPutDir;

    private String packageDir;

    private String tablePrefix;

    private String createFile;

    private Date createDate;

    private Date updateDate;

    private String remarks;

    private String baseEntity;

    @TableLogic
    private String delFlag;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
