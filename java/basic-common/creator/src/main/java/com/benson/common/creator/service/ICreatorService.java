package com.benson.common.creator.service;

import com.benson.common.creator.entity.Creator;
import com.baomidou.mybatisplus.extension.service.IService;

import java.io.FileNotFoundException;

/**
 * <p>
 * 代码生成 服务类
 * </p>
 *
 * @author zhangby
 * @since 2020-02-18
 */
public interface ICreatorService extends IService<Creator> {

    void generator(String id) throws FileNotFoundException;

}
