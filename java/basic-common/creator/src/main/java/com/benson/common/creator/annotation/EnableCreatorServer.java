package com.benson.common.creator.annotation;

import com.benson.common.creator.CreatorApplication;
import org.springframework.context.annotation.Import;

import java.lang.annotation.*;

/**
 * mybatis 创建服务
 *
 * @author zhangby
 * @date 18/2/20 9:39 am
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Inherited
@Import({CreatorApplication.class})
public @interface EnableCreatorServer {

}
