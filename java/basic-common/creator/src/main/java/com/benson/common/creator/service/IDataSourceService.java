package com.benson.common.creator.service;

import cn.hutool.core.lang.Dict;
import com.benson.common.creator.entity.CreatorDataSource;

import java.util.List;

public interface IDataSourceService {
    List<Dict> getTable(CreatorDataSource dataSource) throws Exception;
}
