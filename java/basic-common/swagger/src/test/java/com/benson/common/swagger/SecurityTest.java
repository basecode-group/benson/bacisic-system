package com.benson.common.swagger;

import cn.hutool.core.util.CharsetUtil;
import cn.hutool.crypto.SecureUtil;
import cn.hutool.crypto.symmetric.AES;
import cn.hutool.crypto.symmetric.SymmetricAlgorithm;
import cn.hutool.crypto.symmetric.SymmetricCrypto;

public class SecurityTest {
    public static void main(String[] args) {
        String password = "bacisic_admin_api";
        AES aes = SecureUtil.aes("Bacisic_Swagger_".getBytes());
        String encryptHex = aes.encryptHex(password);
        System.out.println(encryptHex);
        String decryptStr = aes.decryptStr("a0ab2193e656cb7556278d9360df964bc82f86e78a04161a7eb1133adc4febe0");
        System.out.println(decryptStr);
    }
}
