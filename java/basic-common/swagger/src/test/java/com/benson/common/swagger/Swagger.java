package com.benson.common.swagger;

import cn.hutool.http.Header;
import cn.hutool.http.HttpRequest;
import cn.hutool.http.HttpResponse;

public class Swagger {
    public static void main(String[] args) {
        HttpResponse execute = HttpRequest.get("http://localhost:8000/swagger-resources")
                .header(Header.CONTENT_TYPE, "application/json")
                .execute();
        System.out.println(execute.body());
    }
}
