package com.benson.common.swagger.service;

import javax.servlet.http.HttpServletRequest;

public interface ISwaggerAuthService {
    boolean verifyToken(HttpServletRequest request);
}
