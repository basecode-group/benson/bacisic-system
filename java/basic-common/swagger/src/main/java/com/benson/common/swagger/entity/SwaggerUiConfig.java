package com.benson.common.swagger.entity;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * Swagger 配置信息
 *
 * @author zhangby
 * @date 19/2/20 4:56 pm
 */
@Data
@Component
public class SwaggerUiConfig {
    @Value("${basic-common.swagger.username:}")
    private String username;
    @Value("${basic-common.swagger.password:}")
    private String password;
    @Value("${basic-common.swagger.visitUrl:}")
    private String visitUrl;
}
