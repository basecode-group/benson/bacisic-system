package com.benson.common.swagger.annotation;

import com.benson.common.swagger.SwaggerApplication;
import org.springframework.context.annotation.Import;

import java.lang.annotation.*;

/**
 * mybatis 创建服务
 *
 * @author zhangby
 * @date 18/2/20 9:39 am
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Inherited
@Import({SwaggerApplication.class})
public @interface EnableSwaggerUiServer {
}
