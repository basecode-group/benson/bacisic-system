# 基础模块构建项目

> 基于springboot封装的公共服务项目，包含
> - commom 公用模块
> - security 权限模块
> - system 基础代码模块
> - creator 代码生成器模块 
> - cron 定时任务模块

> 可根据需要随时引用快速搭建基础模块项目。