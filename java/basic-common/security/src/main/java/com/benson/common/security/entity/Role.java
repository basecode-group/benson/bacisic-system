package com.benson.common.security.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import java.util.Date;
import java.io.Serializable;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.benson.common.common.entity.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 角色表
 * </p>
 *
 * @author zhangby
 * @since 2020-02-17
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("sys_role")
@ApiModel(value="Role对象", description="角色表")
public class Role extends BaseEntity<Role> {

private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "编号",hidden = true)
    private String id;

    @ApiModelProperty(value = "归属机构")
    private String officeId;

    @ApiModelProperty(value = "角色名称")
    private String name;

    @ApiModelProperty(value = "英文名称")
    private String enname;

    @ApiModelProperty(value = "角色类型")
    private String roleType;

    @ApiModelProperty(value = "数据范围")
    private String dataScope;

    @ApiModelProperty(value = "是否系统数据")
    private String isSys;

    @ApiModelProperty(value = "是否可用")
    private String useable;

    @ApiModelProperty(value = "创建者",hidden = true)
    private String createBy;

    @ApiModelProperty(value = "创建时间",example = "2020-02-17 00:00:00",hidden = true)
    private Date createDate;

    @ApiModelProperty(value = "更新者",hidden = true)
    private String updateBy;

    @ApiModelProperty(value = "更新时间",example = "2020-02-17 00:00:00",hidden = true)
    private Date updateDate;

    @ApiModelProperty(value = "备注信息")
    private String remarks;

    @TableLogic
    @ApiModelProperty(value = "删除标记",example = "0",hidden = true)
    private String delFlag;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
