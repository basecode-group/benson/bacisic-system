package com.benson.common.security;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 授权服务
 *
 * @author zhangby
 * @date 17/2/20 4:14 pm
 */
@SpringBootApplication(scanBasePackages = "com.benson.common.*")
@MapperScan("com.benson.common.security.mapper")
public class SecurityApplication {

    public static void main(String[] args) {
        SpringApplication.run(SecurityApplication.class, args);
    }

}
