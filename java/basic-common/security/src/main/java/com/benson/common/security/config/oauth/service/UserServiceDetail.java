package com.benson.common.security.config.oauth.service;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.benson.common.common.constants.Constants;
import com.benson.common.common.util.CommonUtil;
import com.benson.common.security.entity.OauthRole;
import com.benson.common.security.entity.OauthUser;
import com.benson.common.security.entity.Role;
import com.benson.common.security.entity.User;
import com.benson.common.security.mapper.SecurityRoleDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

/**
 * 加载用户特定数据的核心接口（Core interface which loads user-specific data.）
 *
 * @author zhangby
 * @date 2019-05-14 09:49
 */
@Service
public class UserServiceDetail implements UserDetailsService {

    @Autowired
    SecurityRoleDao securityRoleDao;

    /**
     * 根据用户名查询用户
     */
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        String authType = System.getProperty(Constants.AUTH_TYPE);
        OauthUser oauthUser = new OauthUser();
//        查询登录用户
        User user = new User().selectOne(new LambdaQueryWrapper<User>().eq(User::getLoginName, username));
        Optional.of(user).ifPresent(us ->{
            //查询角色
            List<Role> roleByUser = securityRoleDao.getRoleByUser(user.getId());
            //数据类型转换
            List<OauthRole> oauthRoles = CommonUtil
                    .convers(roleByUser, role -> new OauthRole(
                            role.getId(),
                            Optional.ofNullable(role).map(Role::getEnname).orElse(null))
                    );
            //数据结果集封装
            oauthUser.setId(us.getId());
            oauthUser.setPassword(user.getPassword());
            oauthUser.setUsername(user.getLoginName());
            oauthUser.setOauthRoles(oauthRoles);
        });
        //判断登录入口
        return oauthUser;
    }
}
