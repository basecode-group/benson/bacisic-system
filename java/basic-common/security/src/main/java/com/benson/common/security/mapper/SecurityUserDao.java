package com.benson.common.security.mapper;

import com.benson.common.security.entity.User;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;

/**
 * <p>
 * 用户表  Mapper 接口
 * </p>
 *
 * @author zhangby
 * @since 2020-02-17
 */
@Repository
public interface SecurityUserDao extends BaseMapper<User> {

}
