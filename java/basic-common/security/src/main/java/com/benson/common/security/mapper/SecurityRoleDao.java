package com.benson.common.security.mapper;

import com.benson.common.security.entity.Role;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 * 角色表 Mapper 接口
 * </p>
 *
 * @author zhangby
 * @since 2020-02-17
 */
@Repository
public interface SecurityRoleDao extends BaseMapper<Role> {

    /**
     * query user info by userId
     *
     * @param userId
     * @return List
     */
    List<Role> getRoleByUser(@Param("userId") String userId);
}
