package com.benson.common.security.config.oauth;

import com.benson.common.common.interceptor.JwtInterceptor;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

/**
 * jwt拦截器，默认拦截验证登录用户 token
 *
 * @author zhangby
 * @date 18/1/20 1:51 pm
 */
@Configuration
public class MVCConfig extends WebMvcConfigurerAdapter {

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        /** 注解拦截器 */
        registry.addInterceptor(new JwtInterceptor())
                .addPathPatterns("/**")
                .excludePathPatterns("/static/*","/templates/*");
        super.addInterceptors(registry);
    }
}