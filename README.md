# Common System Tools Doc

<!-- <p align="center">
  <img src="docs/images/logo1.png" style="width:40px" class="no-zoom" />
  <span style="font-size: 14px; font-family: fantasy; font-weight: 600; position: relative; top: -32px; left: -44px;">
    TOOLS
  </span>
</p> -->

<!-- ![](docs/images/logo1.png) -->

### 介绍

基于Springboot封装的公共服务项目，基础架构：`Springboot`, `Spring-Security`, `Spring-oauth2`, `Jwt`, `Quartz`,` Mybatis-plus`, `Hutool`, `Guava`, `Fastjson`, 
`Swagger`, `Mysql`, `Redis`, `Ant-design` 包含：

- `commom` 公用模块
- `security` 权限模块
- `system` 基础代码模块
- `creator` 代码生成器模块 
- `cron` 定时任务模块
- `swagger` API 文档模块
- `upload` 本地文件上传模块

> 可根据需要随时引用快速搭建基础模块项目。

### 文档

- [GitLab](https://basecode-group.gitlab.io/benson/bacisic-system/)
- [Gitee](http://regan_jeff.gitee.io/basic-system/#/)

### 页面展示

#### System 基础架构模块

- 用户管理
<img src="docs/images/system-01.png" style="width:780px" class="no-zoom" />

#### Creator 代码生成器

- 代码生成器
<img src="docs/images/creator-01.png" style="width:780px" class="no-zoom" />

#### Cron API文档

- Cron 定时任务
<img src="docs/images/cron-01.png" style="width:780px" class="no-zoom" />

#### Swagger API文档

- API文档
<img src="docs/images/swagger-01.png" style="width:780px" class="no-zoom" />
